﻿using LibraryBLL.Dto;
using System.Collections.Generic;

namespace LibraryBLL.Services
{
    public interface IAuthorService
    {
        IEnumerable<AuthorDto> GetAllAuthors();
    }
}
