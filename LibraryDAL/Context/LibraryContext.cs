﻿using LibraryDAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryDAL.Context
{
    public class LibraryContext
    {
        public LibraryContext()
        {
            Initialize();
        }

        public IEnumerable<Author> Authors { get; set; }

        public IEnumerable<Book> Books { get; set; }

        public void Initialize()
        {
            // authors 

            Author firstAuthor = new Author()
            {
                FirstName = "Taras",
                LastName = "Shevchenko",
                DateOfBirth = new DateTime(1814, 3, 9),
                DateOfDeath = new DateTime(1861, 3, 10),
                Books = new List<AuthorBook>()
            };
            Author secondAuthor = new Author()
            {
                FirstName = "Alexandr",
                LastName = "Pushkin",
                DateOfBirth = new DateTime(1799, 6, 6),
                DateOfDeath = new DateTime(1861, 3, 10),
                Books = new List<AuthorBook>()
            };
            Author thirdAuthor = new Author()
            {
                FirstName = "Serhii",
                LastName = "Zhadan",
                DateOfBirth = new DateTime(1974, 8, 23),
                DateOfDeath = null,
                Books = new List<AuthorBook>()
            };

            // books

            Book firstBook = new Book()
            {
                Title = "Mesopotamia",
                YearOfPublishing = DateTime.UtcNow.Year,
                Amount = 100,
                Genre = Genre.Fiction,
                Authors = new List<AuthorBook>()
            };
            Book secondBook = new Book()
            {
                Title = "Kateryna",
                YearOfPublishing = DateTime.UtcNow.Year,
                Amount = 1000,
                Genre = Genre.Novel,
                Authors = new List<AuthorBook>()
            };
            Book thirdBook = new Book()
            {
                Title = "Poems",
                YearOfPublishing = DateTime.UtcNow.Year,
                Amount = 1000,
                Genre = Genre.Pomance,
                Authors = new List<AuthorBook>()
            };
            Book fourthBook = new Book()
            {
                Title = "The New York Times",
                YearOfPublishing = DateTime.UtcNow.Year,
                Amount = 10000,
                Genre = Genre.Periodic,
                Authors = null,
            };
            Book fifthBook = new Book()
            {
                Title = "Fifth book",
                YearOfPublishing = DateTime.UtcNow.Year,
                Amount = 10000,
                Genre = Genre.Adventures,
                Authors = new List<AuthorBook>()
            };

            // set relations between author and his books
            firstAuthor.Books.Append(new AuthorBook() { AuthorId = firstAuthor.Id, BookId = secondBook.Id });
            firstAuthor.Books.Append(new AuthorBook() { AuthorId = firstAuthor.Id, BookId = fifthBook.Id });

            secondAuthor.Books.Append(new AuthorBook() { AuthorId = secondAuthor.Id, BookId = thirdBook.Id });
            secondAuthor.Books.Append(new AuthorBook() { AuthorId = secondAuthor.Id, BookId = fifthBook.Id });

            thirdAuthor.Books.Append(new AuthorBook() { AuthorId = thirdAuthor.Id, BookId = firstBook.Id });
            thirdAuthor.Books.Append(new AuthorBook() { AuthorId = thirdAuthor.Id, BookId = fifthBook.Id });

            Authors = new List<Author>()
            {
                firstAuthor,
                secondAuthor,
                thirdAuthor
            };

            // set relation between book and its authors
            firstBook.Authors.Append(new AuthorBook() { BookId = firstBook.Id, AuthorId = thirdAuthor.Id });
            secondBook.Authors.Append(new AuthorBook() { BookId = secondBook.Id, AuthorId = firstAuthor.Id });
            thirdBook.Authors.Append(new AuthorBook() { BookId = thirdBook.Id, AuthorId = secondAuthor.Id });
            // fourth book doesn`t have authors and we put Authors=null
            fifthBook.Authors.Append(new AuthorBook() { BookId = fifthBook.Id, AuthorId = firstAuthor.Id });
            fifthBook.Authors.Append(new AuthorBook() { BookId = fifthBook.Id, AuthorId = secondAuthor.Id });
            fifthBook.Authors.Append(new AuthorBook() { BookId = fifthBook.Id, AuthorId = thirdAuthor.Id });

            Books = new List<Book>()
            {
                firstBook,
                secondBook,
                thirdBook,
                fourthBook,
                fifthBook
            };

        }
    }
}
