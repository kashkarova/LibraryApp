﻿using System;

namespace LibraryDAL.Entities
{
    public class AuthorBook : BaseEntity
    {
        public Guid AuthorId { get; set; }

        public Guid BookId { get; set; }
    }
}
