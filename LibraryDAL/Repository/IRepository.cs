﻿using System;
using System.Collections.Generic;

namespace LibraryDAL.Repository
{
    public interface IRepository<BaseEntity> where BaseEntity : class
    {
        IEnumerable<BaseEntity> GetAll();

        BaseEntity GetEntityById(Guid id);

        bool Create(BaseEntity entity);

        BaseEntity Update(BaseEntity newEntity);

        bool Delete(Guid id);
    }
}