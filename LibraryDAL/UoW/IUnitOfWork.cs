﻿using LibraryDAL.Entities;
using LibraryDAL.Repository;

namespace LibraryDAL.UoW
{
    public interface IUnitOfWork
    {
        public IRepository<Author> AuthorRepository { get; }
    }
}