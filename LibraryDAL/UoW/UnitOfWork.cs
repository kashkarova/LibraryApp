﻿using LibraryDAL.Context;
using LibraryDAL.Entities;
using LibraryDAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryDAL.UoW
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private LibraryContext _libraryContext;
        private IRepository<Author> _authorRepository;

        public UnitOfWork(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }
        
        public IRepository<Author> AuthorRepository
        {
            get
            {
                if(_authorRepository == null)
                {
                    _authorRepository = new AuthorRepository(_libraryContext);
                }

                return _authorRepository;
            }
        }

        public void Dispose()
        {
            // dbContext.Clear();
        }
    }
}
